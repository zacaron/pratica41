/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Circulo;
/**
 *
 * @author zacaron
 */
public class Pratica41 {
    public static void main(String[] args) {
        
        Elipse e = new Elipse(4,2);
        Circulo c = new Circulo(4);
              
        System.out.println("Area da " + e + " = " + e.getArea());
        System.out.println("Perímetro da " + e + " = " + e.getPerimetro());
        System.out.println("Area do " + c + " = " + c.getArea());
        System.out.println("Perímetro do " + c + " = " + c.getPerimetro());
        
    }
    
}
